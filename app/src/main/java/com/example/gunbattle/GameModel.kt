package com.example.gunbattle

import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi

class GameModel {

    private var cells : IntArray
    private var cellNumber = 0
    private var selectedGun = -1
    private var canonBalls = ArrayList<CanonBall>()
    private var viewHeight : Int = 0
    private var viewWidth : Int = 0
    private var powerJauge : Int = 100
    private val handler = Handler(Looper.getMainLooper())


    constructor(cellsAttr: IntArray, cellNumberAttr : Int) {
        cells = cellsAttr
        cellNumber = cellNumberAttr
    }

    companion object {
        fun create(cellNumber: Int, guns: Int, strongholds: Int): GameModel {
            var cells = IntArray(cellNumber)
            cells.fill(1, 0, guns)
            cells.fill(2, guns, guns + strongholds)
            cells.fill(0, guns + strongholds, cellNumber)
            Log.d("size", cells.size.toString())
            cells.shuffle()
            return GameModel(cells, cellNumber)
        }
    }

    fun getCells() : IntArray {
        return cells
    }

    fun getCellNumber() : Int {
        return cellNumber
    }

    fun getSelectedGun() : Int {
        return selectedGun
    }

    fun setSelectedGun(gun : Int) {
        selectedGun = gun
    }

    fun getCanonBalls() : ArrayList<CanonBall>{
        return canonBalls
    }

    fun addCanonBall(canonBall : CanonBall){
        canonBalls.add(canonBall)
    }

    fun getViewHeight() : Int {
        return viewHeight
    }

    fun getViewWidth() : Int {
        return viewWidth
    }

    fun setViewHeight(height : Int) {
        viewHeight = height
    }

    fun setViewWidth(width : Int) {
        viewWidth = width
    }


    @RequiresApi(Build.VERSION_CODES.N)
    fun move() {
        for (c in canonBalls) {
            if (c.y >= 0){
                c.move()
            }
            val cellWidth = viewWidth / cellNumber
            val i = (c.x / cellWidth).toInt()
            if (i < cells.size && i >= 0 && cells[i] != 0 && c.y < 0){
                removeElement(i)
                if (i == selectedGun){
                    selectedGun = -1
                }
            }
        }
        removeCanonballs()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun removeCanonballs(){
        canonBalls.removeIf {it.x > viewWidth || it.x < 0 || it.y < 0}

    }

    private fun removeElement(i : Int) {
        cells[i] = 0;
    }

    fun getPowerJauge() : Int {
        return powerJauge
    }

    fun incrementPowerJauge() {
        if (powerJauge < viewWidth + 100) {
            powerJauge += (viewWidth * 0.02).toInt()
        }
    }

    fun resetPowerJauge() {
        powerJauge = 100
    }

}
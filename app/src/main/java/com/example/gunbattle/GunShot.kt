package com.example.gunbattle

import android.util.Log
import kotlin.math.cos
import kotlin.math.sin

/** Model for the computation of a gun shot
 *  t0: timestamp (in milliseconds) of the launch
 *  x0: position on the x-axis of the launch (meters)
 *  v0: initial speed of the launch (m/s)
 *  angle: angle (with the x-axis) of the launch (radians)
 *  gravity: value of the acceleration of gravity (m/s^2)
 */
data class GunShot(val t0: Long, val x0: Float, val v0: Float, val angle: Float, val gravity: Float) {

    /** Compute the position for time t */
    fun computePosition(t: Long): FloatArray {
        val deltaT = (t - t0) / 1000f // seconds since the launch
        val y = - 0.5f * gravity * deltaT * deltaT + v0 * sin(angle) * deltaT
        val x = v0 * cos(angle) * deltaT + x0
        return floatArrayOf(x, y)
    }
}

data class CanonBall(var x : Float, var y : Float, val gunShot : GunShot) {

    fun move(){
        val pos = gunShot.computePosition(System.currentTimeMillis())
        x = pos[0]
        y = pos[1]
    }
}
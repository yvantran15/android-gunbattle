package com.example.gunbattle

interface GameViewListener {

    fun onGunClick(model : GameModel, gunIndex : Int)

    fun onGunShot(model : GameModel, posX: Float, posY: Float, gunShot : GunShot)


}
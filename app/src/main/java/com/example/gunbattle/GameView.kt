package com.example.gunbattle

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.MotionEvent.ACTION_DOWN
import android.view.View
import android.view.MotionEvent.ACTION_UP
import kotlin.math.atan2

class GameView : View {

    private lateinit var model : GameModel
    private lateinit var gvl : GameViewListener
    private val bitmapGun = BitmapFactory.decodeResource(resources, R.drawable.gun2)
    private val bitmapStronghold = BitmapFactory.decodeResource(resources, R.drawable.tower)
    private var startPressing : Long = 0L

    private val powerJaugeRunnable = Runnable {
        model.incrementPowerJauge()
        handler.postDelayed(getPowerJaugeRunnable(), 40)
    }

    private fun getPowerJaugeRunnable() : Runnable {
        return powerJaugeRunnable
    }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.GameView, defStyle, 0
        )
        a.recycle()
    }



    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (model.getViewHeight() == 0) {
            model.setViewHeight(height)
            model.setViewWidth(width)
        }

        canvas.drawARGB(255, 78, 168, 186)

        val cellWidth = width / model.getCellNumber()
        val cellHeight = height / 10

        for (i in model.getCells().indices){
            var rect = Rect(i*cellWidth, height-cellHeight, i*cellWidth+cellWidth, height)
            when(model.getCells()[i]) {
                1 -> canvas.drawBitmap(bitmapGun, null, rect, null)
                2 -> canvas.drawBitmap(bitmapStronghold, null, rect, null)
            }
        }
        var paint = Paint()
        paint.color = Color.parseColor("#FFFFFF")
        paint.style = Paint.Style.STROKE
        if (model.getSelectedGun() != -1) {
            var rect = Rect(
                model.getSelectedGun() * cellWidth,
                height - cellHeight,
                model.getSelectedGun() * cellWidth + cellWidth,
                height
            )
            canvas.drawRect(rect, paint)
        }
        paint.color = Color.parseColor("#5f5350")
        paint.style = Paint.Style.FILL
        for (c in model.getCanonBalls()){
            canvas.drawCircle(c.x, height - c.y - cellHeight, 30F, paint)
        }
        var paint2 = Paint()
        paint2.color = Color.parseColor("#B75EA4")
        paint2.style = Paint.Style.FILL
        canvas.drawRect(Rect(0, 0, model.getPowerJauge()-100, 80), paint2)
    }



    override fun onTouchEvent(event: MotionEvent?): Boolean {

        val cellWidth = width / model.getCellNumber()
        val cellHeight = height / 10
        if (event != null && event.actionMasked == ACTION_DOWN) {
            startPressing = System.currentTimeMillis()
            var x = (event.x / cellWidth).toInt()
            var y = event.y
            if (y > (height - cellHeight) && model.getCells()[x] == 1) {
                gvl.onGunClick(model, x)
                invalidate()
                return false
            }
            if (model.getSelectedGun() != -1) {
                handler.postDelayed(powerJaugeRunnable, 0)
            }
            return true
        }
        else if(event != null && event.actionMasked == ACTION_UP ) {
            handler.removeCallbacks(powerJaugeRunnable)
            var elapsed = (System.currentTimeMillis() - startPressing)
            if (model.getSelectedGun() != -1 && event.y > 0){

                val h = (height - cellHeight - y)
                val l = (event.x - (model.getSelectedGun()*cellWidth))

                var angle = atan2(h,l)

                val position = (model.getSelectedGun() * cellWidth + cellWidth/2).toFloat()
                val gunShot = GunShot(System.currentTimeMillis(), position, 0.2F*elapsed*10, angle, 160.81F)
                gvl.onGunShot(model, position, height-cellHeight.toFloat(), gunShot)
                model.resetPowerJauge()
            }
        }
        return super.onTouchEvent(event)
    }

    fun setGameViewListener(gameViewListener : GameViewListener) {
        gvl = gameViewListener
    }


    fun setModel(gameModel : GameModel) {
        model = gameModel
        invalidate()
    }


}
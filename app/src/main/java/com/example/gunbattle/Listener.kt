package com.example.gunbattle

class Listener : GameViewListener {


    override fun onGunClick(model : GameModel, gunIndex: Int) {
        model.setSelectedGun(gunIndex)
    }

    override fun onGunShot(model : GameModel, posX: Float, posY: Float, gunShot : GunShot) {
        model.addCanonBall(CanonBall(posX, posY, gunShot))
    }



}
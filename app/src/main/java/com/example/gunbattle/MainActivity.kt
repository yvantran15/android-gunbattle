package com.example.gunbattle

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi

class MainActivity : AppCompatActivity() {

    private var model = GameModel.create(10, 3, 2)

    private val handler = Handler(Looper.getMainLooper())

    @RequiresApi(Build.VERSION_CODES.N)
    private val drawCanonBall = Runnable {
        model.move()
        val view = findViewById<GameView>(R.id.gameView)
        if (!tryFinish()) {
            view.invalidate()
            handler.postDelayed(getCanonBallRunnable(), 40)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun getCanonBallRunnable() : Runnable{
        return drawCanonBall
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onCreateHelper()
    }

    fun onCreateHelper() {
        setContentView(R.layout.activity_main)
        val view = findViewById<GameView>(R.id.gameView)
        view.setModel(model)
        val listener = Listener()
        view.setGameViewListener(listener)
        handler.postDelayed(drawCanonBall, 0)
    }

    private fun tryFinish() : Boolean{
        if (!(model.getCells().contains(2))) {
            displayDialog(resources.getString(R.string.win))
            return true
        }
        else if (!(model.getCells().contains(1))) {
            displayDialog(resources.getString(R.string.lose))
            return true
        }
        return false
    }

    private fun displayDialog(msg : String) {
        AlertDialog.Builder(this)
            .setMessage(msg)
            .setCancelable(false)
            .setPositiveButton("Yes"
            ) { _, _ ->
                model = GameModel.create(10, 3, 2)
                onCreateHelper()
            }
            .setNegativeButton("No"
            ) { _, _ ->
                finishAffinity()
            }
            .show()
    }


}